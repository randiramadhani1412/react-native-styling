import React from 'react';
import {View, StyleSheet, Image, Text,} from 'react-native';
const Home = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          width: 300,
          height: 500,
          borderRadius: 10,
          overflow: 'hidden',
          elevation: 10,
        }}>
        <Image
          style={{
            flex: 1,
            width: 300,
            resizeMode: 'cover',
          }}
          source={require('../assets/logo.png')}
        />
        <View
          style={{
            width: 300,
            backgroundColor: 'white',
            flexDirection: 'column',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'black'}}>
            Styling Di React Native
          </Text>
          <Text style={{fontSize: 16, color: 'gray'}}>
            Binar Academy - React Native
          </Text>
          <Text style={{fontSize: 16, textAlign: 'justify', color: 'black'}}>
            as component grows in complexity, it is much cleaner and efficient
            to use StyleSheet create so as define several styles in one place.
          </Text>
          <View
            style={{
              marginVertical: 20,
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 16, color: 'blue'}}>Understod!</Text>
            <Text style={{fontSize: 16, color: 'blue'}}>What?!!</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Home;
